#include "app/application.h"

int main(int argc, char const *argv[]) {
	return app::Application::run(argc, argv);
}